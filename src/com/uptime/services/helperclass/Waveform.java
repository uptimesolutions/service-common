/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.uptime.services.helperclass;

import java.time.Instant;
import java.util.List;
import java.util.Objects;

/**
* The Waveform entity represents the waveform User Defined Type whose primary purpose it to contain an array of sample values.
* 
 * @author madhavi
*/
public class Waveform {
    private Instant sampleTime;
    private String deviceId;
    private byte channelNum;
    private float sampleRateHz;
    private String sensitivityUnits;
    private List<Double> samples;

    public Waveform() {
    }

    public Instant getSampleTime() {
        return sampleTime;
    }

    public void setSampleTime(Instant sampleTime) {
        this.sampleTime = sampleTime;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public byte getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(byte channelNum) {
        this.channelNum = channelNum;
    }

    public float getSampleRateHz() {
        return sampleRateHz;
    }

    public void setSampleRateHz(float sampleRateHz) {
       this.sampleRateHz = sampleRateHz;
    }

    public String getSensitivityUnits() {
        return sensitivityUnits;
    }

    public void setSensitivityUnits(String sensitivityUnits) {
        this.sensitivityUnits = sensitivityUnits;
    }

    public List<Double> getSamples() {
        return samples;
    }

    public void setSamples(List<Double> samples) {
        this.samples = samples;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.sampleTime);
        hash = 13 * hash + Objects.hashCode(this.deviceId);
        hash = 13 * hash + this.channelNum;
        hash = 13 * hash + Float.floatToIntBits(this.sampleRateHz);
        hash = 13 * hash + Objects.hashCode(this.sensitivityUnits);
        hash = 13 * hash + Objects.hashCode(this.samples);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Waveform other = (Waveform) obj;
        if (this.channelNum != other.channelNum) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRateHz) != Float.floatToIntBits(other.sampleRateHz)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.sensitivityUnits, other.sensitivityUnits)) {
            return false;
        }
        if (!Objects.equals(this.sampleTime, other.sampleTime)) {
            return false;
        }
        if (!Objects.equals(this.samples, other.samples)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"sampleTime\" : \"" + sampleTime + "\", \"deviceId\" : \"" + deviceId + "\", \"channelNum\" : \"" + channelNum + "\", \"sampleRateHz\" : \"" + sampleRateHz + "\", \"sensitivityUnits\" : \"" + sensitivityUnits + "\", \"samples\" : [" + samples + "]}";
    }
    
}

