/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services.helperclass;

import java.time.Instant;
import java.util.Objects;

/**
 *
 * @author twilcox
 */
public class WorkOrderRecord {
    private String clientAssetId;
    private String areaName;
    private String assetComponent;
    private String assetName;
    private byte priority;
    private Instant closeDate;
    private String action;
    private String description;
    private String issue;
    private String notes;
    private String user;

    public WorkOrderRecord() {
    }

    public String getClientAssetId() {
        return clientAssetId;
    }

    public void setClientAssetId(String clientAssetId) {
        this.clientAssetId = clientAssetId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetComponent() {
        return assetComponent;
    }

    public void setAssetComponent(String assetComponent) {
        this.assetComponent = assetComponent;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public byte getPriority() {
        return priority;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }

    public Instant getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Instant closeDate) {
        this.closeDate = closeDate;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + Objects.hashCode(this.clientAssetId);
        hash = 11 * hash + Objects.hashCode(this.areaName);
        hash = 11 * hash + Objects.hashCode(this.assetComponent);
        hash = 11 * hash + Objects.hashCode(this.assetName);
        hash = 11 * hash + this.priority;
        hash = 11 * hash + Objects.hashCode(this.closeDate);
        hash = 11 * hash + Objects.hashCode(this.action);
        hash = 11 * hash + Objects.hashCode(this.description);
        hash = 11 * hash + Objects.hashCode(this.issue);
        hash = 11 * hash + Objects.hashCode(this.notes);
        hash = 11 * hash + Objects.hashCode(this.user);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkOrderRecord other = (WorkOrderRecord) obj;
        if (this.priority != other.priority) {
            return false;
        }
        if (!Objects.equals(this.clientAssetId, other.clientAssetId)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetComponent, other.assetComponent)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.action, other.action)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.issue, other.issue)) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.closeDate, other.closeDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder json;
        
        json = new StringBuilder();
        json
                .append("{\"clientAssetId\":\"").append(clientAssetId).append("\",")
                .append("\"areaName\":\"").append(areaName).append("\",")
                .append("\"assetComponent\":\"").append(assetComponent).append("\",")
                .append("\"assetName\":\"").append(assetName).append("\",")
                .append("\"priority\":").append(priority).append(",")
                .append("\"closeDate\":\"").append(closeDate).append("\",")
                .append("\"action\":\"").append(action).append("\",")
                .append("\"description\":\"").append(description).append("\",")
                .append("\"issue\":\"").append(issue).append("\",")
                .append("\"notes\":\"").append(notes).append("\",")
                .append("\"user\":\"").append(user).append("\"}");
        return json.toString();
    }
}
