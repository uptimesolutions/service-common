/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services.vo;

import java.io.Serializable;

/**
 *
 * @author twilcox
 */
public class EventVO implements Serializable {
    private String application;
    private String data;
    private String ipAddress;
    private String port;
    private Long createdDate;

    public EventVO() {
    }

    public EventVO(EventVO eventVO) {
        application = eventVO.getApplication();
        data = eventVO.getData();
        ipAddress = eventVO.getIpAddress();
        port = eventVO.getPort();
        createdDate = eventVO.getCreatedDate();
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }
    
}
