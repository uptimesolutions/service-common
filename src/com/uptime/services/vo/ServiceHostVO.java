/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services.vo;

import java.io.Serializable;

/**
 *
 * @author ksimmons
 */
public class ServiceHostVO implements Serializable {
    private String ip;
    private int port;
    private boolean circuitBreaker;
    
    public ServiceHostVO() {
        port = -1;
        circuitBreaker = false;
    }
    
    public ServiceHostVO(ServiceHostVO serviceHostVO) {
        ip = serviceHostVO.getIp();
        port = serviceHostVO.getPort();
        circuitBreaker = serviceHostVO.isCircuitBreaker();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isCircuitBreaker() {
        return circuitBreaker;
    }

    public void setCircuitBreaker(boolean circuitBreaker) {
        this.circuitBreaker = circuitBreaker;
    }
    
}
