/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services;

import com.uptime.services.vo.ServiceHostVO;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.addServiceHostList;
import static com.uptime.services.util.ServiceUtil.ipAddressCheck;
import static com.uptime.services.util.ServiceUtil.portCheck;
import static com.uptime.services.util.ServiceUtil.updateServiceHostList;
import com.uptime.services.vo.EventVO;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author twilcox
 */
public abstract class AbstractService {

    private volatile static Map<String, ArrayList<ServiceHostVO>> serviceHosts = new HashMap();
    private final static AtomicLong threadCount = new AtomicLong(0);
    private static final int READ_TIMEOUT = 5000;
    private static final int CONNECTION_TIMEOUT = 5000;

    /**
     * @return the serviceHosts
     */
    public static Map<String, ArrayList<ServiceHostVO>> getServiceHosts() {
        return serviceHosts;
    }

    /**
     * @return the requiredServices as a long
     */
    public static long getThreadCount() {
        return threadCount.get();
    }

    /**
     * Increments and returns the threadCount
     *
     * @return long
     */
    public static long incrementThreadCount() {
        return threadCount.incrementAndGet();
    }

    /**
     * Decrements and returns the threadCount
     *
     * @return long
     */
    public static long decrementThreadCount() {
        return threadCount.decrementAndGet();
    }

    /**
     * Registers this service with the Service Registrar.
     *
     * @param name, String Object
     * @param ipAddress, String Object
     * @param port, int
     * @param logger, Logger Object
     * @return boolean, true if the service registered successfully otherwise
     * false
     */
    public static boolean register(String name, String ipAddress, int port, Logger logger) {
        RequestConfig config;
        StringBuilder json;
        HttpPost httpPost;
        int status = -1;

        try {
            // create the JSON to post
            json = new StringBuilder();
            json.append("{\"name\":\"").append(name).append("\",")
                    .append("\"ipAddress\":\"").append(ipAddress).append("\",")
                    .append("\"port\":\"").append(port).append("\"}");

            logger.log(Level.INFO, "Connecting to Registrar at: {0}/register", ServiceConstants.REGISTRAR_URL);
            logger.log(Level.INFO, "Registering: Service:{0} Host:{1}:{2}", new Object[]{name, ipAddress, String.valueOf(port)});

            // set the connection timeouts
            config = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setConnectionRequestTimeout(READ_TIMEOUT)
                    .setSocketTimeout(READ_TIMEOUT).build();

            // create the POST request
            httpPost = new HttpPost(ServiceConstants.REGISTRAR_URL + "/register");

            // add the JSON to the request
            httpPost.setEntity(new StringEntity(json.toString()));
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // send the request
            // get the status code
            try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                    CloseableHttpResponse response = httpClient.execute(httpPost)) {
                status = response.getStatusLine().getStatusCode();
            }

            if (status == 200) {
                return true;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        logger.log(Level.SEVERE, "HTTP RESPONSE:{0}", status);
        return false;
    }

    /**
     * Sends an Event to the Events service
     *
     * @param event, EventVO object
     * @param ipAddress, String object, ipAddress of the Events service host
     * @param port, int, port of the Events Service host
     * @return boolean, true if the event sent successfully, otherwise false
     */
    public static boolean sendEvent(EventVO event, String ipAddress, int port) {
        RequestConfig config;
        HttpPost httpPost;
        StringBuilder json;
        int status = -1;

        try {
            // create the JSON to POST to the Event service         
            json = new StringBuilder();
            json
                    .append("{\"application\":\"").append(event.getApplication()).append("\",")
                    .append("\"created_date\":\"").append(event.getCreatedDate()).append("\",");
            if (ipAddressCheck(event.getIpAddress())) {
                json.append("\"ip_address\":\"").append(event.getIpAddress()).append("\",");
            }
            if (portCheck(event.getPort())) {
                json.append("\"port\":\"").append(event.getPort()).append("\",");
            }
            json.append("\"data\":\"").append(event.getData()).append("\"}");

            // set the connection timeouts
            config = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setConnectionRequestTimeout(READ_TIMEOUT)
                    .setSocketTimeout(READ_TIMEOUT).build();

            // create the POST request
            httpPost = new HttpPost("http://" + ipAddress + ":" + port + "/events");

            // add the JSON to the request
            httpPost.setEntity(new StringEntity(json.toString()));
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // send the request
            // get the status code
            try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                    CloseableHttpResponse response = httpClient.execute(httpPost)) {
                status = response.getStatusLine().getStatusCode();
            }

            if (status == 200) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("SendEvent Exception: " + e.getMessage());
        }
        return false;
    }

    /**
     * Unregisters this service with the Service Registrar.
     *
     * @param name, String Object
     * @param ipAddress, String Object
     * @param port, int
     * @param logger, Logger Object
     * @return boolean, true if the service unregistered successfully otherwise
     * false
     */
    public static boolean unregister(String name, String ipAddress, int port, Logger logger) {
        RequestConfig config;
        HttpDelete httpDelete;
        StringBuilder url;
        int status = -1;

        try {
            url = new StringBuilder();
            url
                    .append(ServiceConstants.REGISTRAR_URL).append("/register")
                    .append("?name=").append(name)
                    .append("&port=").append(port)
                    .append("&ipAddress=").append(ipAddress);

            // set the connection timeouts
            config = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setConnectionRequestTimeout(READ_TIMEOUT)
                    .setSocketTimeout(READ_TIMEOUT).build();

            httpDelete = new HttpDelete(url.toString());
            httpDelete.setHeader("Accept", "application/json");

            // send the request
            // get the status code
            try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                    CloseableHttpResponse response = httpClient.execute(httpDelete)) {
                status = response.getStatusLine().getStatusCode();
            }

            if (status == 200) {
                return true;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        logger.log(Level.SEVERE, "HTTP RESPONSE:{0}", status);
        return false;
    }

    /**
     * Subscribe to which notifications are needed and to where they need to be
     * sent
     *
     * @param names, Array Object of String Objects
     * @param ipAddress, String Object
     * @param port, int
     * @param url, String Object
     * @param source, String Object
     * @param logger, Logger Object
     * @return boolean, true if the service subscribes successfully otherwise
     * false
     */
    public static boolean subscribe(String[] names, String ipAddress, int port, String url, String source, Logger logger) {
        RequestConfig config;
        StringBuilder json;
        HttpPost httpPost;
        int status = -1;

        try {
            json = new StringBuilder();
            json.append("{\"names\":[");
            for (int i = 0; i < names.length; i++) {
                json.append("{\"name\":\"").append(names[i]).append(i < names.length - 1 ? "\"}," : "\"}");
            }
            json
                    .append("],\"ipAddress\":\"").append(ipAddress).append("\",")
                    .append("\"url\":\"").append(url).append("\",")
                    .append("\"port\":\"").append(port).append("\",")
                    .append("\"source\":\"").append(source).append("\"}");

            // set the connection timeouts
            config = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setConnectionRequestTimeout(READ_TIMEOUT)
                    .setSocketTimeout(READ_TIMEOUT).build();

            // create the POST request
            httpPost = new HttpPost(ServiceConstants.REGISTRAR_URL + "/subscribe");

            // add the JSON to the request
            httpPost.setEntity(new StringEntity(json.toString()));
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // send the request
            // get the status code
            try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                    CloseableHttpResponse response = httpClient.execute(httpPost)) {
                status = response.getStatusLine().getStatusCode();
            }

            if (status == 200) {
                return true;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        logger.log(Level.SEVERE, "HTTP RESPONSE:{0}", status);
        return false;
    }

    /**
     * Unsubscribe to receive notifications
     *
     * @param names, Array Object of String Objects
     * @param ipAddress, String Object
     * @param port, int
     * @param url, String Object
     * @param source, String Object
     * @param logger, Logger Object
     * @return boolean, true if the service unsubscribes successfully otherwise
     * false
     */
    public static boolean unsubscribe(String[] names, String ipAddress, int port, String url, String source, Logger logger) {
        RequestConfig config;
        StringBuilder json;
        HttpPut httpPut;
        int status = -1;

        try {
            json = new StringBuilder();
            json.append("{\"names\":[");
            for (int i = 0; i < names.length; i++) {
                json.append("{\"name\":\"").append(names[i]).append(i < names.length - 1 ? "\"}," : "\"}");
            }
            json
                    .append("],\"ipAddress\":\"").append(ipAddress).append("\",")
                    .append("\"url\":\"").append(url).append("\",")
                    .append("\"port\":\"").append(port).append("\",")
                    .append("\"source\":\"").append(source).append("\"}");

            // set the connection timeouts
            config = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setConnectionRequestTimeout(READ_TIMEOUT)
                    .setSocketTimeout(READ_TIMEOUT).build();

            // create the Put request
            httpPut = new HttpPut(ServiceConstants.REGISTRAR_URL + "/subscribe");

            // add the JSON to the request
            httpPut.setEntity(new StringEntity(json.toString()));
            httpPut.setHeader("Accept", "application/json");
            httpPut.setHeader("Content-type", "application/json");

            // send the request
            // get the status code
            try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                    CloseableHttpResponse response = httpClient.execute(httpPut)) {
                status = response.getStatusLine().getStatusCode();
            }

            if (status == 200) {
                return true;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        logger.log(Level.SEVERE, "HTTP RESPONSE:{0}", status);
        return false;
    }

    /**
     * Informs the Service Monitor that due to network letency a circuit breaker
     * has been enableThe Service Monitor would then POST /circuitbreaker
     * messages to all service instances on the network.dk.
     *
     * @param host, ServiceHostVO Object
     * @param name, String Object
     * @param logger, Logger Object
     * @return boolean, true if CircuitBreaker post success, else false
     */
    public static boolean publishCircuitBreaker(ServiceHostVO host, String name, Logger logger) {
        RequestConfig config;
        StringBuilder json;
        HttpPost httpPost;
        int status = -1;

        try {
            json = new StringBuilder();
            json
                    .append("{\"name\":\"").append(name).append("\",")
                    .append("\"ipAddress\":\"").append(host.getIp()).append("\",")
                    .append("\"port\":\"").append(host.getPort()).append("\"}");

            // set the connection timeouts
            config = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setConnectionRequestTimeout(READ_TIMEOUT)
                    .setSocketTimeout(READ_TIMEOUT).build();

            // create the POST request
            httpPost = new HttpPost(ServiceConstants.REGISTRAR_URL + "/circuitbreaker");

            // add the JSON to the request
            httpPost.setEntity(new StringEntity(json.toString()));
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // send the request
            // get the status code
            try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                    CloseableHttpResponse response = httpClient.execute(httpPost)) {
                status = response.getStatusLine().getStatusCode();
            }

            if (status == 200) {
                return true;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        logger.log(Level.SEVERE, "HTTP RESPONSE:{0}", status);
        return false;
    }

    /**
     * Gets the following statistics for this service instance: - Current number
     * of requests being handled by this service instance - Total Memory - Free
     * Memory - Total number of threads
     *
     * @return String object, Json to be sent
     * @throws Exception
     */
    public static String getStatistics() throws Exception {
        StringBuilder sb = new StringBuilder();
        sb
                .append("{\"currentRequests\":\"").append(threadCount).append("\",")
                .append("\"totalMemory\":\"").append(Runtime.getRuntime().totalMemory() / 1024L / 1024L).append("M\",")
                .append("\"freeMemory\":\"").append(Runtime.getRuntime().freeMemory() / 1024L / 1024L).append("M\",")
                .append("\"threads\":\"").append(ManagementFactory.getThreadMXBean().getThreadCount()).append("\"}");
        return sb.toString();
    }

    /**
     * Returns the service info
     *
     * @param name, String Object
     * @param ipAddress, String Object
     * @param port, int
     * @return String object, Json to be sent
     */
    public static String getServiceInfo(String name, String ipAddress, int port) {
        StringBuilder sb = new StringBuilder();
        sb
                .append("{\"name\":\"").append(name).append("\",")
                .append("\"ipAddress\":\"").append(ipAddress).append("\",")
                .append("\"port\":\"").append(port).append("\"}");
        return sb.toString();
    }

    /**
     * Gets a list of instances for the request service.
     *
     * @param name, String Object
     * @param logger, Logger Object
     * @return boolean, true if Query is success, else false
     */
    public static boolean query(String name, Logger logger) {
        StringBuilder url;
        RequestConfig config;
        HttpGet httpGet;
        HttpEntity entity;
        int status = -1;
        String result = null;

        try {
            url = new StringBuilder();
            url.append(ServiceConstants.REGISTRAR_URL).append("/query?name=").append(name);

            // set the connection timeouts
            config = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setConnectionRequestTimeout(READ_TIMEOUT)
                    .setSocketTimeout(READ_TIMEOUT).build();

            // create the GET request
            httpGet = new HttpGet(url.toString());
            httpGet.setHeader("Accept", "application/json");

            // send the request
            // get the status code
            try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                    CloseableHttpResponse response = httpClient.execute(httpGet)) {
                status = response.getStatusLine().getStatusCode();
                if ((entity = response.getEntity()) != null) {
                    result = EntityUtils.toString(entity);
                }
            }

            // parse the response
            if (status == 200) {
                if (serviceHosts.containsKey(name)) {
                    serviceHosts.replace(name, updateServiceHostList(result, serviceHosts.get(name)));
                } else {
                    serviceHosts.put(name, addServiceHostList(result));
                }
                return true;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        logger.log(Level.SEVERE, "HTTP RESPONSE:{0}", status);
        return false;
    }

    /**
     * Updates the serviceHosts field
     *
     * @param content, String object
     * @return boolean, true if update is successful, else false
     */
    public static boolean updateServiceHosts(String content) {
        String name;
        JsonElement element;
        JsonObject json;

        try {
            if (content != null && (element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("hosts") && json.has("name") && (name = json.get("name").getAsString()) != null && !name.isEmpty()) {
                if (serviceHosts.containsKey(name)) {
                    serviceHosts.replace(name, updateServiceHostList(content, serviceHosts.get(name)));
                } else {
                    serviceHosts.put(name, addServiceHostList(content));
                }
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    /**
     * Instructs the service to enable a circuit breaker for the given service
     * instance.
     *
     * @param content, String Object
     * @return String Object
     * @throws Exception
     */
    public static String addCircuitBreaker(String content) throws Exception {
        JsonElement element;
        JsonObject json;

        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("name") && json.has("ipAddress") && json.has("port")
                && json.get("name") != null && !json.get("name").getAsString().isEmpty() && ipAddressCheck(json.get("ipAddress").getAsString()) && portCheck(json.get("port").getAsInt())) {
            serviceHosts.get(json.get("name").getAsString()).stream().filter(s -> s.getIp().equals(json.get("ipAddress").getAsString()) && s.getPort() == json.get("port").getAsInt()).forEach(s -> s.setCircuitBreaker(true));
            return "Notification: CircuitBreaker added for: " + json.get("name").getAsString() + ", Ip: " + json.get("ipAddress").getAsString() + ", Port: " + json.get("port").getAsString();
        }
        return null;
    }

    /**
     * Instructs the service to disable a circuit breaker for the given service
     * instance.
     *
     * @param uriQuery, String Object
     * @return String Object
     * @throws Exception
     */
    public static String clearCircuitBreaker(String uriQuery) throws Exception {
        Map<String, Object> params;

        if (uriQuery != null && (params = parseQuery(uriQuery)) != null && !params.isEmpty() && params.containsKey("name") && params.containsKey("ipAddress") && params.containsKey("port")
                && params.get("name") != null && !params.get("name").toString().isEmpty() && ipAddressCheck(params.get("ipAddress").toString()) && portCheck(params.get("port").toString())) {
            serviceHosts.get(params.get("name").toString()).stream().filter(s -> s.getIp().equals(params.get("ipAddress").toString()) && s.getPort() == Integer.parseInt(params.get("port").toString())).forEach(s -> s.setCircuitBreaker(false));
            return "Notification: CircuitBreaker cleared for: " + params.get("name").toString() + ", Ip: " + params.get("ipAddress").toString() + ", Port: " + params.get("port").toString();
        }
        return null;
    }

    /**
     * Instructs the service to add a registered service
     *
     * @param content, String Object
     * @param logger, Logger Object
     * @return String Object
     * @throws Exception
     */
    public static String addRegistration(String content, Logger logger) throws Exception {
        JsonElement element;
        JsonObject json;
        String name;

        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("name")) {
            if (!query(name = json.get("name").getAsString(), logger)) {
                System.out.println("Query failed for " + name);
            }
            return name;
        }
        return null;
    }

    /**
     * Instructs the service to clear a registered service
     *
     * @param uriQuery, String Object
     * @return String Object
     * @throws Exception
     */
    public static String clearRegistration(String uriQuery) throws Exception {
        Map<String, Object> params;
        String name;

        if (uriQuery != null && (params = parseQuery(uriQuery)) != null && !params.isEmpty() && params.containsKey("name") && params.get("name") != null && !params.get("name").toString().isEmpty()) {
            serviceHosts.remove(name = params.get("name").toString());
            return name;
        }
        return null;
    }

    // subclasses must provide the implementation
    public abstract void sendEmail(String json);
}

