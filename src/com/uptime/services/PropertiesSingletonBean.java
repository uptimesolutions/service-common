/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author akumar
 */
public class PropertiesSingletonBean {
    private static final PropertiesSingletonBean INSTANCE = new PropertiesSingletonBean();
    private final Properties properties;
    private String env = null;
    
    /**
     * Private Constructor
     */
    private PropertiesSingletonBean() {
        properties = new Properties();
        env = null;
        
        try (InputStream is = new FileInputStream(new File(System.getenv("PROP_FILE_LOCATION")))) {
            properties.load(is);
            env = properties.getProperty("env");
            if(env == null || env.isEmpty())
                throw new Exception("Env variable missing");
        } catch(Exception ex) {
            Logger.getLogger(PropertiesSingletonBean.class.getName()).log(Level.SEVERE, "Fail to load properties file", ex);
            System.exit(0);
        } 
    }

    /**
     * return a instance of the class 
     * @return PropertiesSingletonBean Object
     */
    public static PropertiesSingletonBean getInstance() {
        return INSTANCE;
    }
    
    /**
     * return the property based on the given key
     * @param key, String object
     * @return String Object
     */
    public String getProperty(String key) {
        return properties.getProperty(key + env);
    }
    
    /**
     * return the property based on the given key
     * @param key, String object
     * @return String Object
     */
    public boolean getBooleanProperty(String key) {
        String value;
        
        try {
            if ((value = properties.getProperty(key + env)) == null || value.isEmpty()) {
                throw new Exception("Missing boolean properties from property file. Value \"" + key + env + "\", not found.");
            } else {
                return Boolean.parseBoolean(value);
            }
        } catch (Exception ex) {
            Logger.getLogger(PropertiesSingletonBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(0);
        }
        
        return false;
    }
        
}
