/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services;

/**
 *
 * @author ksimmons
 */
public class ServiceConstants {
    public final static String REGISTRAR_URL = PropertiesSingletonBean.getInstance().getProperty("REGISTRAR_URL");
    
    public final static String PEER_REGISTRAR_URL_LIST = PropertiesSingletonBean.getInstance().getProperty("PEER_REGISTRAR_URL_LIST");
            
    public final static String DEFAULT_UPTIME_ACCOUNT = PropertiesSingletonBean.getInstance().getProperty("DEFAULT_UPTIME_ACCOUNT");
    
    // **** IMPORTANT ****
    // If this is set to false for the serviceRegistrar then only IPs starting with 10.1.30 will work. All other IPs will send a 406 response code 
    public final static boolean DEVELOPING_TESTING = PropertiesSingletonBean.getInstance().getBooleanProperty("DEVELOPING_TESTING");
    
    public final static boolean ENABLE_KAFKA_NOTIFICATIONS = PropertiesSingletonBean.getInstance().getBooleanProperty("ENABLE_KAFKA_NOTIFICATIONS");
}
