/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services.http.handler;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import static com.uptime.services.AbstractService.decrementThreadCount;
import static com.uptime.services.AbstractService.incrementThreadCount;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public abstract class AbstractGenericHandler implements HttpHandler {
    
    /**
     * HttpHandler handler
     * @param he
     */
    @Override
    public void handle(HttpExchange he) {
        incrementThreadCount();
        try {
            if(getRunning()) {
                switch(he.getRequestMethod()){
                    case "GET":
                        doGet(he);
                        break;
                    case "POST":
                        doPost(he);
                        break;
                    case "PUT":
                        doPut(he);
                        break;
                    case "DELETE":
                        doDelete(he);
                        break;
                    default:
                        he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
                        he.close();
                }  
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, 0);
                he.close();
            }
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, e.getMessage(), e);
        }
        decrementThreadCount();
    }
    
    /**
     * HTTP GET handler
     * @param he
     * @throws IOException 
     */
    public void doGet(HttpExchange he) throws IOException {
        OutputStream os;
        Headers respHeaders;
        byte[] response;
        
        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        response = "{\"outcome\":\"HTTP GET not implemented.\"}".getBytes();
        he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_IMPLEMENTED, response.length);
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();
    }
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException 
     */
    public void doPost(HttpExchange he) throws IOException {
        he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_IMPLEMENTED, 0);
        he.close();
    }
    
    /**
     * HTTP Put handler
     * @param he
     * @throws IOException 
     */
    public void doPut(HttpExchange he) throws IOException {
        he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_IMPLEMENTED, 0);
        he.close();
    }

    /**
     * HTTP DELETE handler
     * @param he
     * @throws IOException 
     */
    public void doDelete(HttpExchange he) throws IOException { 
        he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_IMPLEMENTED, 0);
        he.close();
    }
    
    public abstract boolean getRunning();
    public abstract Logger getLogger();
}
