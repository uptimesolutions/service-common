/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services.http.handler;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import static com.uptime.services.AbstractService.decrementThreadCount;
import static com.uptime.services.AbstractService.getServiceInfo;
import static com.uptime.services.AbstractService.getStatistics;
import static com.uptime.services.AbstractService.incrementThreadCount;
import static com.uptime.services.AbstractService.register;
import static com.uptime.services.AbstractService.subscribe;
import static com.uptime.services.AbstractService.unregister;
import static com.uptime.services.AbstractService.unsubscribe;
import static com.uptime.services.AbstractService.updateServiceHosts;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.getServiceCommand;
import static com.uptime.services.util.ServiceUtil.streamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public abstract class AbstractSystemHandler implements HttpHandler {
    /**
     * HttpHandler handler
     * @param he 
     */
    @Override
    public void handle(HttpExchange he) {
        incrementThreadCount();
        try {
            if(getRunning()) {
                switch(he.getRequestMethod()){
                    case "GET":
                        doGet(he);
                        break;
                    case "POST":
                        doPost(he);
                        break;
                    case "PUT":
                        doPut(he);
                        break;
                    default:
                        he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
                        he.close();
                }  
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, 0);
                he.close();
            }
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        decrementThreadCount();
    }
    
    /**
     * HTTP GET handler
     * @param he
     * @throws IOException 
     */
    private void doGet(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        Map<String, Object> params;
        byte [] response;
        OutputStream os;
        Headers respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        String resp;
                
        try {
            params = parseQuery(he.getRequestURI().getQuery());
            if (params.isEmpty()) {
                resp = getServiceInfo(getServiceName(), getServiceIpAddress(), getServicePort());
            } else if(params.get("serviceCommand") != null){
                resp = params.get("serviceCommand").toString().equalsIgnoreCase("activity") ? getStatistics() : "{\"outcome\":\"Unknown Value\"}";
            } else {
                resp = "{\"outcome\":\"Unknown Params\"}";
            }
            
            if(resp.contains("Unknown Params") || resp.contains("Unknown Value")) { 
                response = resp.getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            } else {
                response = resp.getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            }
        } catch (Exception e) {
            stackTrace = e.getStackTrace();
            response = "{\"outcome\":\"Exception\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            getLogger().log(Level.SEVERE, e.getMessage(), e);
        }
        
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();
        
        // Send Event
        if(stackTrace != null) 
            sendEvent(stackTrace);
    }
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException 
     */
    private void doPost(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        String content, command, resp = null;

        try {
            if((content = streamReader(he.getRequestBody())) != null && !content.isEmpty()) {
                if((command = getServiceCommand(content)) != null) {
                    switch(command.toLowerCase()) {
                        case"shutdown":
                            resp = "Start shutdown successfully.";
                            setRunning(false);
                            break;
                        case"unregister":
                            resp = unregister(getServiceName(), getServiceIpAddress(), getServicePort(), getLogger()) ? "Unregistering successfully." : "Unregistering failed.";
                            break;
                        case"register":
                            resp = register(getServiceName(), getServiceIpAddress(), getServicePort(), getLogger()) ? "Registering successfully." : "Registering failed.";
                            break;
                        case"unsubscribe":
                            resp = unsubscribe(getServiceSubscribeNames(), getServiceIpAddress(), getServicePort(), "", "service", getLogger()) ? "Unsubscribing successfully." : "Unsubscribing failed.";
                            break;
                        case"subscribe":
                            resp = subscribe(getServiceSubscribeNames(), getServiceIpAddress(), getServicePort(), "", "service", getLogger()) ? "Subscribing successfully." : "Subscribing failed.";
                            break;
                    }
                }
            }
            if(resp == null) { 
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            } else if (resp.contains(" successfully.")) {
                System.out.println(resp);
                getLogger().log(Level.INFO, resp);
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            } else {
                System.out.println(resp);
                getLogger().log(Level.WARNING, resp);
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            }
        } catch (Exception e) {
            stackTrace = e.getStackTrace();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            getLogger().log(Level.SEVERE, e.getMessage(), e);
        }
        he.close();
        
        // Send Event
        if(stackTrace != null)
            sendEvent(stackTrace);
    }
    
    /**
     * HTTP PUT handler
     * @param he
     * @throws IOException 
     */
    private void doPut(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        boolean response;
        
        try {
            try { 
                getMutex().acquire();
                response = updateServiceHosts(streamReader(he.getRequestBody()));
            } finally {
                getMutex().release();
            }
            if (response) {
                System.out.println("Update to serviceHosts received.");
                getLogger().log(Level.INFO, "Update to serviceHosts received.");
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            } else {
                System.out.println("Update to serviceHosts failed");
                getLogger().log(Level.WARNING, "Update to serviceHosts failed");
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
            }
        } catch (Exception e) {
            stackTrace = e.getStackTrace();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            getLogger().log(Level.SEVERE, e.getMessage(), e);
        }
        he.close();
        
        // Send Event
        if(stackTrace != null)
            sendEvent(stackTrace);
    }
    
    public abstract boolean getRunning();
    public abstract void setRunning(boolean value);
    public abstract Logger getLogger();
    public abstract String getServiceName();
    public abstract int getServicePort();
    public abstract String getServiceIpAddress();
    public abstract String[] getServiceSubscribeNames();
    public abstract Semaphore getMutex();
    public abstract void sendEvent(StackTraceElement[] stackTrace);
}
