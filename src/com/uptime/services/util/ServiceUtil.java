/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.services.vo.ServiceHostVO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author twilcox
 */
public class ServiceUtil {
  
    /**
     * Checks if a given ip starts with the network of 10.1.10 or 10.2.10
     * @param ipAddress, String object
     * @return boolean, true if check succeeds, else false
     */
    public static boolean networkCheck(String ipAddress){
        return ipAddressCheck(ipAddress) && (ipAddress.startsWith("10.1.10.") || ipAddress.startsWith("10.2.10."));
    }
    
    /**
     * Checks if the given ip address is valid
     * @param ipAddress, String object
     * @return boolean, true if check succeeds, else false
     */
    public static boolean ipAddressCheck(String ipAddress) {
        Pattern pattern;
        
        if (ipAddress != null) {
            pattern = Pattern.compile("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
            return pattern.matcher(ipAddress).matches();
        }
        return false;
    }
    
    /**
     * Checks if the given port is valid
     * @param port, Object
     * @return boolean, true if check succeeds, else false
     */
    public static boolean portCheck(Object port) {
        int value;
        
        if (port != null) {
            if (port instanceof Integer) {
                value = (Integer)port;
                return value > -1 && value <= 65535;
            } else if (port instanceof String) {
                value = Integer.parseInt((String)port);
                return value > -1 && value <= 65535;
            }
        }
        return false;
    }
    
    /**
     * Checks if the given source is valid
     * @param source, String object
     * @return boolean, true if check succeeds, else false
     */
    public static boolean sourceCheck(String source) {
        if (source != null) {
            return source.equalsIgnoreCase("service") || source.equalsIgnoreCase("gui");
        }
        return false;
    }
    
    /**
     * Check the given String Object if it can be converted to a boolean
     * @param value, String object
     * @return boolean, true if check succeeds, else false
     */
    public static boolean booleanCheck(String value) {
        if (value != null) {
            return value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false");
        }
        return false;
    }
    
    public static String getServiceCommand(String content) throws Exception {
        JsonElement element;
        JsonObject json;
        
        if(content != null && (element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("serviceCommand") && json.get("serviceCommand") != null) {
            return json.get("serviceCommand").getAsString();
        }
        return null;
    }
    
    /**
     * Updates a prexisting ServiceHostVO list.
     * @param content String object of the given json
     * @param list of ServiceHostVO objects, to be updated
     * @return list of ServiceHostVO objects
     * @throws Exception 
     */
    public static ArrayList<ServiceHostVO> updateServiceHostList (String content, ArrayList<ServiceHostVO> list) throws Exception{
        JsonElement element;
        JsonObject json;
        JsonArray hosts;
        
        if(content != null && (element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("hosts")) {
            list = new ArrayList();
            if((hosts = json.getAsJsonArray("hosts")) != null){
                for (int i = 0; i < hosts.size(); i++) {
                    json = hosts.get(i).getAsJsonObject();
                    if (json.has("ipAddress") && json.has("port") && json.has("circuitBreaker")) {
                        if (ipAddressCheck(json.get("ipAddress").getAsString()) && portCheck(json.get("port").getAsInt()) && booleanCheck(json.get("circuitBreaker").getAsString())) {
                            ServiceHostVO vo = new ServiceHostVO();
                            vo.setIp(json.get("ipAddress").getAsString());
                            vo.setPort(json.get("port").getAsInt());
                            vo.setCircuitBreaker(json.get("circuitBreaker").getAsBoolean());
                            list.add(vo);
                        }
                    }
                }
            } 
        }
        return list;
    }
    
    /**
     * Adds a new ServiceHostVO list.
     * @param content String object of the given json
     * @return list of ServiceHostVO objects
     * @throws Exception 
     */
    public static ArrayList<ServiceHostVO> addServiceHostList (String content) throws Exception{
        ArrayList<ServiceHostVO> list = new ArrayList();
        JsonElement element;
        JsonObject json;
        JsonArray hosts;
        
        if(content != null && (element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("hosts") && (hosts = json.getAsJsonArray("hosts")) != null){
            for (int i = 0; i < hosts.size(); i++) {
                json = hosts.get(i).getAsJsonObject();
                if (json.has("ipAddress") && json.has("port") && json.has("circuitBreaker")) {
                    if (ipAddressCheck(json.get("ipAddress").getAsString()) && portCheck(json.get("port").getAsInt()) && booleanCheck(json.get("circuitBreaker").getAsString())) {
                        ServiceHostVO vo = new ServiceHostVO();
                        vo.setIp(json.get("ipAddress").getAsString());
                        vo.setPort(json.get("port").getAsInt());
                        vo.setCircuitBreaker(json.get("circuitBreaker").getAsBoolean());
                        list.add(vo);
                    }
                }
            }
        }
        return list;
    }
    
    /**
     * Converts an InputString to a String
     * @param stream, InputStream object
     * @return String
     * @throws IOException 
     */
    public static String streamReader(InputStream stream) throws IOException{
        String requestBody;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(stream, "utf-8"))) {
            requestBody = in.lines().collect(Collectors.joining(System.lineSeparator()));
        } 
        return requestBody;
    }
    
    /**
     * Check the entity to make sure at least one none PK field isn't null
     * @param <T> 
     * @param t, Any Entity Class
     * @return String Object
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException 
     */
    public static <T> String validateObjectData(T t) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        boolean pkNotAvailable = false;
        boolean primitiveFound = false;
        if (t != null) {
            String pkMethodName;
            List<String> pkMethodList = new ArrayList();
            for (Field f : t.getClass().getDeclaredFields()) {
                Annotation[] annotationArr = f.getAnnotations();
                String annotation = annotationArr[0].toString();
                if (annotation.contains("PartitionKey") || annotation.contains("ClusteringColumn")) {
                    if (f.getType().toString().equals("boolean")) {
                        pkMethodName = "is" + f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1);
                    } else {
                        pkMethodName = "get" + f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1);
                    }
                    pkMethodList.add(pkMethodName);
                }
                if (!f.getType().toString().contains("class")) {
                    primitiveFound = true;
                }

            }
            //first check if all PK have their corresponding value 
            for (Method method : t.getClass().getDeclaredMethods()) {
                if (Modifier.isPublic(method.getModifiers())
                        && method.getParameterTypes().length == 0
                        && method.getReturnType() != void.class
                        && (method.getName().startsWith("get") || method.getName().startsWith("is"))) {
                    if (pkMethodList.contains(method.getName())) {
                        Object value = method.invoke(t);
                        //check if any of the pk is null or empty
                        if (value == null || value.toString().isEmpty()) {
                            pkNotAvailable = true;
                        }
                    }

                }
            }

            if (pkNotAvailable) {
                return "{\"outcome\":\"Insufficient and/or invalid data for PK given in json\"}";
            }
            if (!primitiveFound) {
                for (Method method : t.getClass().getDeclaredMethods()) {
                    if (Modifier.isPublic(method.getModifiers())
                            && method.getParameterTypes().length == 0
                            && method.getReturnType() != void.class
                            && (method.getName().startsWith("get"))) {
                        
                        //check for no PK and non pimitive method data
                        if (!pkMethodList.contains(method.getName())) {
                            Object value = method.invoke(t);
                            //check if at least one method value is not null
                            if (value != null) {
                                return null;
                            }
                        }

                    }
                }
            } else {
                return null;
            }
            
            return "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";

        }

        return "{\"outcome\":\"Json is invalid\"}";
    }
    
    /**
     * Check the list of entities to make sure at least one none PK field isn't null
     * @param <T> 
     * @param tList, List of  Entity Objects
     * @param allowNullList boolean to represent whether a null list is valid or not
     * @return String Object
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException 
     */
    public static <T> String validateObjects(List<T> tList, boolean allowNullList) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

        if (tList != null && !tList.isEmpty()) {
            List<T> validList = new ArrayList();
            for (T t : tList) {
                if (validateObjectData(t) == null) {
                    validList.add(t);
                }
            }
            if (validList.size() == tList.size())
                return null;
        }

        if (allowNullList)
            return null;
        else            
            return "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
    }
}

