/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services.util;

import static com.uptime.services.util.JsonConverterUtil.DATE_TIME_OFFSET_FORMATTER;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author ksimmons
 */
public class HttpUtils {
    
    public static HashMap<String, Object> parseQuery(String query) throws UnsupportedEncodingException{
        HashMap<String, Object> params = new HashMap();
        String[] keyValuePair;
        List<String> values;
        String key, value;
        Object object;
        
        if (query != null) {
            for (String pair : query.split("[&]")) {
                keyValuePair = pair.split("[=]");
                key = keyValuePair.length > 0 ? URLDecoder.decode(keyValuePair[0],System.getProperty("file.encoding")) : null;
                value = keyValuePair.length > 1 ? URLDecoder.decode(keyValuePair[1],System.getProperty("file.encoding")): null;
                
                if(params.containsKey(key)) {
                    object = params.get(key);
                    if (object instanceof List<?>) {
                        values = (List<String>)object;
                        values.add(value);
                    } else if(object instanceof String) {
                        values = new ArrayList();
                        values.add((String)object);
                        values.add(value);
                        params.put(key, values);
                    }
                } else {
                    params.put(key, value);
                }
            }
        }
        return params;
    }
    
    /**
     * Parse a String into an Instant
     * @param value, String Object
     * @return Instant Object
     * @throws Exception 
     */
    public static Instant parseInstant(String value) throws Exception {
        try {
            return Instant.parse(value).atZone(ZoneOffset.UTC).toInstant();
        } catch (Exception e) {
            return ZonedDateTime.parse(value, DATE_TIME_OFFSET_FORMATTER).withZoneSameInstant(ZoneOffset.UTC).toInstant();
        }
    }
    
    /**
     * Parse a String into an Instant
     * @param value, String Object
     * @param zoneId, ZoneId Object
     * @return Instant Object
     * @throws Exception 
     */
    public static Instant parseInstant(String value, ZoneId zoneId) throws Exception {
        try {
            return Instant.parse(value).atZone(zoneId).toInstant();
        } catch (Exception e) {
            return ZonedDateTime.parse(value, DATE_TIME_OFFSET_FORMATTER).withZoneSameInstant(zoneId).toInstant();
        }
    }
}
