/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services.util;

import resources.EchoDiagnosticsTestEntity;
import com.uptime.services.vo.ServiceHostVO;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author twilcox
 */
public class ServiceUtilTest {
    
    /**
     * Test of networkCheck method, of class ServiceUtil.
     */
    @Test
    public void testNetworkCheck() {
        System.out.println("networkCheck method testing.");
                
        assertTrue(ServiceUtil.networkCheck("10.1.10.10"));
        assertFalse(ServiceUtil.networkCheck("10.1.10.10.10"));
        assertFalse(ServiceUtil.networkCheck("11.1.10.10"));
        
        System.out.println("networkCheck method completed.");
    }

    /**
     * Test of ipAddressCheck method, of class ServiceUtil.
     */
    @Test
    public void testIpAddressCheck() {
        System.out.println("ipAddressCheck method testing.");
                
        assertTrue(ServiceUtil.ipAddressCheck("10.1.30.10"));
        assertFalse(ServiceUtil.ipAddressCheck("10.1.30.10.10"));
        
        System.out.println("ipAddressCheck method completed.");
    }

    /**
     * Test of portCheck method, of class ServiceUtil.
     */
    @Test
    public void testPortCheck() {
        System.out.println("portCheck method testing.");
                
        assertFalse(ServiceUtil.portCheck(null));
        assertFalse(ServiceUtil.portCheck(10L));
        assertFalse(ServiceUtil.portCheck(-3));
        assertFalse(ServiceUtil.portCheck(70000));
        assertTrue(ServiceUtil.portCheck(5000));
        assertTrue(ServiceUtil.portCheck("5000"));
        
        System.out.println("portCheck method completed.");
    }

    /**
     * Test of sourceCheck method, of class ServiceUtil.
     */
    @Test
    public void testSourceCheck() {
        System.out.println("sourceCheck method testing.");
                
        assertFalse(ServiceUtil.sourceCheck(null));
        assertFalse(ServiceUtil.sourceCheck("_service"));
        assertTrue(ServiceUtil.sourceCheck("service"));
        assertTrue(ServiceUtil.sourceCheck("gui"));
        assertTrue(ServiceUtil.sourceCheck("serVice"));
        assertTrue(ServiceUtil.sourceCheck("guI"));
        
        System.out.println("sourceCheck method completed.");
    }

    /**
     * Test of booleanCheck method, of class ServiceUtil.
     */
    @Test
    public void testBooleanCheck() {
        System.out.println("booleanCheck method testing.");
                
        assertFalse(ServiceUtil.booleanCheck(null));
        assertFalse(ServiceUtil.booleanCheck("_false"));
        assertTrue(ServiceUtil.booleanCheck("false"));
        assertTrue(ServiceUtil.booleanCheck("true"));
        assertTrue(ServiceUtil.booleanCheck("falSe"));
        assertTrue(ServiceUtil.booleanCheck("True"));
        
        System.out.println("booleanCheck method completed.");
    }

    /**
     * Test of getServiceCommand method, of class ServiceUtil.
     */
    @Test
    public void testGetServiceCommand() {
        System.out.println("getServiceCommand method testing.");
        String content;
        
        try {
        content = "{\"serviceCommand\":\"testCommand\"}";
        assertEquals("testCommand", ServiceUtil.getServiceCommand(content));
        assertNull(ServiceUtil.getServiceCommand(null));
        
        content = "{\"_serviceCommand\":\"testCommand\"}";
        assertNull(ServiceUtil.getServiceCommand(content));
        } catch (Exception ex) {
            fail("getServiceCommand method failed");
        }
        
        System.out.println("getServiceCommand method completed.");
    }

    /**
     * Test of getServiceCommand method Exception, of class ServiceUtil.
     * @throws Exception
     */
    @Test(expected=Exception.class)
    public void testGetServiceCommandException() throws Exception {
        System.out.println("getServiceCommand method Exception test.");
        ServiceUtil.getServiceCommand("");
    }

    /**
     * Test of updateServiceHostList method, of class ServiceUtil.
     */
    @Test
    public void testUpdateServiceHostList() {
        System.out.println("updateServiceHostList method testing.");
        ArrayList<ServiceHostVO> list;
        StringBuilder content;
        
        content = new StringBuilder();
        content
            .append("{\"hosts\":[")
            .append("{\"ipAddress\":\"1.1.1.1\",")
            .append("\"port\":8080,")
            .append("\"circuitBreaker\":true}]}");
        try {
            list = ServiceUtil.updateServiceHostList(content.toString(), null);
            assertEquals(1, list.size());
            assertEquals("1.1.1.1", list.get(0).getIp());
            assertEquals(8080, list.get(0).getPort());
            assertTrue(list.get(0).isCircuitBreaker());
            assertNull(ServiceUtil.updateServiceHostList(null, null));
        } catch (Exception ex) {
            fail("updateServiceHostList method failed");
        }
        System.out.println("updateServiceHostList method completed.");
    }

    /**
     * Test of updateServiceHostList method Exception, of class ServiceUtil.
     * @throws Exception
     */
    @Test(expected=Exception.class)
    public void testUpdateServiceHostListException() throws Exception {
        System.out.println("updateServiceHostList method Exception test.");
        ServiceUtil.updateServiceHostList("", null);
    }

    /**
     * Test of addServiceHostList method, of class ServiceUtil.
     */
    @Test
    public void testAddServiceHostList() {
        System.out.println("addServiceHostList method testing.");
        ArrayList<ServiceHostVO> list;
        StringBuilder content;
        
        content = new StringBuilder();
        content
            .append("{\"hosts\":[")
            .append("{\"ipAddress\":\"1.1.1.1\",")
            .append("\"port\":8080,")
            .append("\"circuitBreaker\":true}]}");
        try {
            list = ServiceUtil.addServiceHostList(content.toString());
            assertEquals(1, list.size());
            assertEquals("1.1.1.1", list.get(0).getIp());
            assertEquals(8080, list.get(0).getPort());
            assertTrue(list.get(0).isCircuitBreaker());
            assertEquals(new ArrayList(), ServiceUtil.addServiceHostList(null));
        } catch (Exception ex) {
            fail("addServiceHostList method failed");
        }
        System.out.println("addServiceHostList method completed.");
    }

    /**
     * Test of addServiceHostList method Exception, of class ServiceUtil.
     * @throws Exception
     */
    @Test(expected=Exception.class)
    public void testAddServiceHostListException() throws Exception {
        System.out.println("addServiceHostList method Exception test.");
        ServiceUtil.addServiceHostList("");
    }

    /**
     * Test of streamReader method, of class ServiceUtil.
     */
    @Test
    public void testStreamReader() {
        System.out.println("streamReader method testing.");
        String content;
        
        try {
            content = "{\"streamReader\":\"This is a test.\"}";
            assertEquals(content, ServiceUtil.streamReader(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8))));
        } catch (IOException ex) {
            fail("streamReader method failed");
        }
        System.out.println("streamReader method completed.");
    }
    
    /**
     * Test of testValidateObjectData method, of class ServiceUtil.
     */
    @Test
    public void test01_ValidateObjectData() {
        System.out.println("testValidateObjectData method testing.");
        EchoDiagnosticsTestEntity entity = new EchoDiagnosticsTestEntity();
        entity.setCustomerAccount("77777");
        entity.setDeviceId("BB001234");
        entity.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        
        try {
            assertEquals(null, ServiceUtil.validateObjectData(entity));
        } catch (Exception ex) {
            fail("streamReader method failed");
        }
        System.out.println("testValidateObjectData method completed.");
    }
    
    /**
     * Test of testValidateObjectData method, of class ServiceUtil.
     */
    @Test
    public void test02_ValidateObjectData() {
        System.out.println("testValidateObjectData method testing.");
        EchoDiagnosticsTestEntity echoStatus = new EchoDiagnosticsTestEntity();
        echoStatus.setCustomerAccount("77777");
        echoStatus.setDeviceId("BB001234");
        
        try {
            String expResult = "{\"outcome\":\"Insufficient and/or invalid data for PK given in json\"}";
            String result = ServiceUtil.validateObjectData(echoStatus);
            assertEquals(expResult, result);
        } catch (Exception ex) {
            fail("streamReader method failed");
        }
        System.out.println("testValidateObjectData method completed.");
    }
    
}

