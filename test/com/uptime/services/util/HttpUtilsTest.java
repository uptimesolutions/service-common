/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services.util;

import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author twilcox
 */
public class HttpUtilsTest {

    /**
     * Test of parseQuery method, of class HttpUtils.
     */
    @Test
    public void testParseQuery() {
        System.out.println("parseQuery method testing.");
        String query = "param1=value1&param1=value2&param2=value3";
        HashMap<String, Object> result;
                
        try {
            result = HttpUtils.parseQuery(query);
            assertEquals(2, result.size());
            assertTrue(result.containsKey("param1"));
            assertTrue(result.containsKey("param2"));
            assertEquals(2, ((List<String>)result.get("param1")).size());
            assertEquals("value1", ((List<String>)result.get("param1")).get(0));
            assertEquals("value2", ((List<String>)result.get("param1")).get(1));
            assertEquals("value3", (String)result.get("param2"));
            assertTrue(HttpUtils.parseQuery(null).isEmpty());
        } catch (UnsupportedEncodingException ex) {
            fail("parseQuery method failed");
        }
        
        System.out.println("parseQuery method completed.");
    }

    /**
     * Test of parseInstant(String) method, of class HttpUtils.
     */
    @Test
    public void testParseInstant_Params_String() {
        System.out.println("parseInstant method testing.");
        Instant result;
        
        try {
            result = HttpUtils.parseInstant("2021-01-01T01:00:00Z");
            assertEquals("2021-01-01T01:00:00Z", result.toString());
            result = HttpUtils.parseInstant("2021-01-01T01:00:00+0000");
            assertEquals("2021-01-01T01:00:00Z", result.toString());
        } catch (Exception ex) {
            fail("parseInstant method failed");
        }
        System.out.println("parseInstant method completed.");
    }

    /**
     * Test of parseInstant(String) method, of class HttpUtils.
     * @throws Exception
     */
    @Test(expected=Exception.class)
    public void testParseInstant_Params_String_Exception() throws Exception {
        System.out.println("parseInstant method Exception test.");
        HttpUtils.parseInstant(null);
    }

    /**
     * Test of parseInstant(String, ZoneId) method, of class HttpUtils.
     */
    @Test
    public void testParseInstant_Params_StringZoneId() {
        System.out.println("parseInstant method testing.");
        Instant result;
        
        try {
            result = HttpUtils.parseInstant("2021-01-01T01:00:00Z", ZoneOffset.UTC);
            assertEquals("2021-01-01T01:00:00Z", result.toString());
            result = HttpUtils.parseInstant("2021-01-01T01:00:00+0000", ZoneOffset.UTC);
            assertEquals("2021-01-01T01:00:00Z", result.toString());
        } catch (Exception ex) {
            fail("parseInstant method failed");
        }
        System.out.println("parseInstant method completed.");
    }

    /**
     * Test of parseInstant(String, ZoneId) method, of class HttpUtils.
     * @throws Exception
     */
    @Test(expected=Exception.class)
    public void testParseInstant_Params_StringZoneId_Exception() throws Exception {
        System.out.println("parseInstant method Exception test.");
        HttpUtils.parseInstant(null, null);
    }
}
