/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.services.util;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author twilcox
 */
public class JsonConverterUtilTest {
    final DateTimeFormatter DATE_TIME_OFFSET_FORMAT;
    final DateTimeFormatter DATE_TIME_FORMATTER;
    final DateTimeFormatter DATE_FORMATTER;
    final ZoneId ZONE_ID; 
    private final TestVO TEST_OBJECT;
    
    /**
     * Private Value Object Class for testing
     */
    private class TestVO {
        private String string_1;
        private String string_2;
        private LocalDateTime localDateTime;
        private LocalDate localDate;
        private Instant instant;

        public String getString_1() {
            return string_1;
        }

        public void setString_1(String string_1) {
            this.string_1 = string_1;
        }

        public String getString_2() {
            return string_2;
        }

        public void setString_2(String string_2) {
            this.string_2 = string_2;
        }

        public LocalDateTime getLocalDateTime() {
            return localDateTime;
        }

        public void setLocalDateTime(LocalDateTime localDateTime) {
            this.localDateTime = localDateTime;
        }

        public LocalDate getLocalDate() {
            return localDate;
        }

        public void setLocalDate(LocalDate localDate) {
            this.localDate = localDate;
        }

        public Instant getInstant() {
            return instant;
        }

        public void setInstant(Instant instant) {
            this.instant = instant;
        }
    }

    /**
     * Constructor
     */
    public JsonConverterUtilTest() {
        DATE_TIME_OFFSET_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");
        DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        DATE_FORMATTER= DateTimeFormatter.ofPattern("yyyy-MM-dd");
        ZONE_ID = ZoneId.systemDefault();
        
        TEST_OBJECT = new TestVO();
        TEST_OBJECT.setString_1("Test_String_1");
        TEST_OBJECT.setString_2("Test_String_2");
        TEST_OBJECT.setLocalDateTime(LocalDateTime.now());
        TEST_OBJECT.setLocalDate(LocalDate.now());
        TEST_OBJECT.setInstant(LocalDateTime.now().atZone(ZoneOffset.UTC).toInstant());
    }

    /**
     * Test of toJSON(Object) method, of class JsonConverterUtil.
     */
    @Test
    public void testToJSON_Params_Object() {
        System.out.println("testToJSON_Params_Object method testing.");
        
        try {
        JsonObject result = JsonParser.parseString(JsonConverterUtil.toJSON(TEST_OBJECT)).getAsJsonObject();

        assertEquals("Test_String_1",result.get("string_1").getAsString());
        assertEquals("Test_String_2",result.get("string_2").getAsString());
        assertEquals(TEST_OBJECT.getLocalDateTime().format(DATE_TIME_FORMATTER),result.get("localDateTime").getAsString());
        assertEquals(TEST_OBJECT.getLocalDate().format(DATE_FORMATTER),result.get("localDate").getAsString());
        assertEquals(DATE_TIME_OFFSET_FORMAT.withZone(ZoneOffset.UTC).format(TEST_OBJECT.getInstant()),result.get("instant").getAsString());
        } catch (Exception ex) {
            fail("testToJSON_Params_Object method failed");
        }
        System.out.println("testToJSON_Params_Object method completed.");
    }

    /**
     * Test of toJSON(Object) method Exception, of class ServiceUtil.
     * @throws Exception
     */
    @Test(expected=Exception.class)
    public void testToJSON_Params_Object_Exception() throws Exception {
        System.out.println("testToJSON_Params_Object_Exception method Exception test.");
        JsonConverterUtil.toJSON(null);
    }

    /**
     * Test of toJSON(Object,ZoneId) method, of class JsonConverterUtil.
     */
    @Test
    public void testToJSON_Params_ObjectZoneId() {
        System.out.println("testToJSON_Params_ObjectZoneId method testing.");
        
        try {
            JsonObject result = JsonParser.parseString(JsonConverterUtil.toJSON(TEST_OBJECT, ZONE_ID)).getAsJsonObject();

            assertEquals("Test_String_1",result.get("string_1").getAsString());
            assertEquals("Test_String_2",result.get("string_2").getAsString());
            assertEquals(TEST_OBJECT.getLocalDateTime().format(DATE_TIME_FORMATTER),result.get("localDateTime").getAsString());
            assertEquals(TEST_OBJECT.getLocalDate().format(DATE_FORMATTER),result.get("localDate").getAsString());
            assertEquals(DATE_TIME_OFFSET_FORMAT.withZone(ZONE_ID).format(TEST_OBJECT.getInstant()),result.get("instant").getAsString());
        } catch (Exception ex) {
            fail("testToJSON_Params_ObjectZoneId method failed");
        }
        System.out.println("testToJSON_Params_ObjectZoneId method completed.");
    }

    /**
     * Test of toJSON(Object,ZoneId) method Exception, of class ServiceUtil.
     * @throws Exception
     */
    @Test(expected=Exception.class)
    public void testToJSON_Params_ObjectZoneId_Exception() throws Exception {
        System.out.println("testToJSON_ObjectZoneId_ObjectZoneId_Exception method Exception test.");
        JsonConverterUtil.toJSON(null, ZONE_ID);
    }

    /**
     * Test of toJSON(Object,String,Object) method, of class JsonConverterUtil.
     */
    @Test
    public void testToJSON_Params_ObjectStringObject() {
        System.out.println("testToJSON_Params_ObjectStringObject method testing.");
        JsonObject result;
        
        try {
            result = JsonParser.parseString(JsonConverterUtil.toJSON(TEST_OBJECT, "string_3", "Test_String_3")).getAsJsonObject();
            
            assertEquals("Test_String_1",result.get("string_1").getAsString());
            assertEquals("Test_String_2",result.get("string_2").getAsString());
            assertEquals("Test_String_3",result.get("string_3").getAsString());
            assertEquals(TEST_OBJECT.getLocalDateTime().format(DATE_TIME_FORMATTER),result.get("localDateTime").getAsString());
            assertEquals(TEST_OBJECT.getLocalDate().format(DATE_FORMATTER),result.get("localDate").getAsString());
            assertEquals(DATE_TIME_OFFSET_FORMAT.withZone(ZoneOffset.UTC).format(TEST_OBJECT.getInstant()),result.get("instant").getAsString());
        } catch (Exception ex) {
            fail("testToJSON_Params_ObjectStringObject method failed");
        }
        System.out.println("testToJSON_Params_ObjectStringObject method completed.");
    }

    /**
     * Test of toJSON(Object,String,Object) method Exception, of class ServiceUtil.
     * @throws Exception
     */
    @Test(expected=Exception.class)
    public void testToJSON_Params_ObjectStringObject_Exception() throws Exception {
        System.out.println("testToJSON_Params_ObjectStringObject_Exception method Exception test.");
        JsonConverterUtil.toJSON(null, "", null);
    }

    /**
     * Test of toJSON(Object,String,Object,ZoneId) method, of class JsonConverterUtil.
     */
    @Test
    public void testToJSON_Params_ObjectStringObjectZoneId() {
        System.out.println("testToJSON_Params_ObjectStringObjectZoneId method testing.");
        JsonObject result;
        
        try {
            result = JsonParser.parseString(JsonConverterUtil.toJSON(TEST_OBJECT, "string_3", "Test_String_3", ZONE_ID)).getAsJsonObject();
            
            assertEquals("Test_String_1",result.get("string_1").getAsString());
            assertEquals("Test_String_2",result.get("string_2").getAsString());
            assertEquals("Test_String_3",result.get("string_3").getAsString());
            assertEquals(TEST_OBJECT.getLocalDateTime().format(DATE_TIME_FORMATTER),result.get("localDateTime").getAsString());
            assertEquals(TEST_OBJECT.getLocalDate().format(DATE_FORMATTER),result.get("localDate").getAsString());
            assertEquals(DATE_TIME_OFFSET_FORMAT.withZone(ZONE_ID).format(TEST_OBJECT.getInstant()),result.get("instant").getAsString());
        } catch (Exception ex) {
            fail("testToJSON_Params_ObjectStringObjectZoneId method failed");
        }
        System.out.println("testToJSON_Params_ObjectStringObjectZoneId method completed.");
    }

    /**
     * Test of toJSON(Object,String,Object,ZoneId) method Exception, of class ServiceUtil.
     * @throws Exception
     */
    @Test(expected=Exception.class)
    public void testToJSON_Params_ObjectStringObjectZoneId_Exception() throws Exception {
        System.out.println("testToJSON_Params_ObjectStringObjectZoneId_Exception method Exception test.");
        JsonConverterUtil.toJSON(null, "", null, null);
    }

    /**
     * Test of toJSON(Object,Map) method, of class JsonConverterUtil.
     */
    @Test
    public void testToJSON_Params_ObjectMap() {
        System.out.println("testToJSON_Params_ObjectMap method testing.");
        JsonObject result;
        Map<String, Object> params;
        
        try {
            params = new HashMap();
            params.put("string_3", "Test_String_3");
            params.put("string_4", "Test_String_4");
            
            result = JsonParser.parseString(JsonConverterUtil.toJSON(TEST_OBJECT, params)).getAsJsonObject();
            
            assertEquals("Test_String_1",result.get("string_1").getAsString());
            assertEquals("Test_String_2",result.get("string_2").getAsString());
            assertEquals("Test_String_3",result.get("string_3").getAsString());
            assertEquals("Test_String_4",result.get("string_4").getAsString());
            assertEquals(TEST_OBJECT.getLocalDateTime().format(DATE_TIME_FORMATTER),result.get("localDateTime").getAsString());
            assertEquals(TEST_OBJECT.getLocalDate().format(DATE_FORMATTER),result.get("localDate").getAsString());
            assertEquals(DATE_TIME_OFFSET_FORMAT.withZone(ZoneOffset.UTC).format(TEST_OBJECT.getInstant()),result.get("instant").getAsString());
        } catch (Exception ex) {
            fail("testToJSON_Params_ObjectMap method failed");
        }
        System.out.println("testToJSON_Params_ObjectMap method completed.");
    }

    /**
     * Test of toJSON(Object,Map) method Exception, of class ServiceUtil.
     * @throws Exception
     */
    @Test(expected=Exception.class)
    public void testToJSON_Params_ObjectMap_Exception() throws Exception {
        System.out.println("testToJSON_Params_ObjectMap_Exception method Exception test.");
        JsonConverterUtil.toJSON(null, new HashMap());
    }

    /**
     * Test of toJSON(Object,Map,ZoneId) method, of class JsonConverterUtil.
     */
    @Test
    public void testToJSON_Params_ObjectMapZoneId() {
        System.out.println("testToJSON_Params_ObjectMapZoneId method testing.");
        JsonObject result;
        Map<String, Object> params;
        
        try {
            params = new HashMap();
            params.put("string_3", "Test_String_3");
            params.put("string_4", "Test_String_4");
            
            result = JsonParser.parseString(JsonConverterUtil.toJSON(TEST_OBJECT, params, ZONE_ID)).getAsJsonObject();
            
            assertEquals("Test_String_1",result.get("string_1").getAsString());
            assertEquals("Test_String_2",result.get("string_2").getAsString());
            assertEquals("Test_String_3",result.get("string_3").getAsString());
            assertEquals("Test_String_4",result.get("string_4").getAsString());
            assertEquals(TEST_OBJECT.getLocalDateTime().format(DATE_TIME_FORMATTER),result.get("localDateTime").getAsString());
            assertEquals(TEST_OBJECT.getLocalDate().format(DATE_FORMATTER),result.get("localDate").getAsString());
            assertEquals(DATE_TIME_OFFSET_FORMAT.withZone(ZONE_ID).format(TEST_OBJECT.getInstant()),result.get("instant").getAsString());
        } catch (Exception ex) {
            fail("testToJSON_Params_ObjectMapZoneId method failed");
        }
        System.out.println("testToJSON_Params_ObjectMapZoneId method completed.");
    }

    /**
     * Test of toJSON(Object,Map,ZoneId) method Exception, of class ServiceUtil.
     * @throws Exception
     */
    @Test(expected=Exception.class)
    public void testToJSON_Params_ObjectMapZoneId_Exception() throws Exception {
        System.out.println("testToJSON_Params_ObjectMapZoneId_Exception method Exception test.");
        JsonConverterUtil.toJSON(null, new HashMap(), ZONE_ID);
    }
}
