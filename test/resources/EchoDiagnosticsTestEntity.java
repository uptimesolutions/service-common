/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.time.Instant;
import java.util.Objects;
import java.util.UUID;
import com.datastax.oss.driver.api.mapper.annotations.ClusteringColumn;
import com.datastax.oss.driver.api.mapper.annotations.CqlName;
import com.datastax.oss.driver.api.mapper.annotations.Entity;
import com.datastax.oss.driver.api.mapper.annotations.PartitionKey;

/**
 *
 * @author kpati
 */
@Entity
public class EchoDiagnosticsTestEntity {
    
    @PartitionKey(0)
    @CqlName(value = "customer_acct")
    private String customerAccount;

    @PartitionKey(1)
    @CqlName(value = "site_id")
    private UUID siteId;

    @ClusteringColumn(0)
    @CqlName(value = "device_id")
    private String deviceId;

    @CqlName(value = "echo_fw")
    String echoFw ; 
    
    @CqlName(value = "echo_datasets")
    short echoDatasets;
    
    @CqlName(value = "echo_backlog")
    short echoBacklog; 
            
    @CqlName(value = "echo_radio_fail")
    byte echoRadioFail;
    
    @CqlName(value = "echo_terr")
    byte echoTerr; 
                    
    @CqlName(value = "echo_restart")
    short echoRestart;
    
    @CqlName(value = "echo_pow_level")
    byte echoPowLevel; 

    @CqlName(value = "echo_reboot")
    short echoReboot;
    
    @CqlName(value = "echo_powerfault")
    short echoPowerfault;
    
    @CqlName(value = "last_update")
    Instant lastUpdate; //use echobase_time from json

    public EchoDiagnosticsTestEntity() {
    }

    public EchoDiagnosticsTestEntity(EchoDiagnosticsTestEntity entity) {
        this.customerAccount = entity.getCustomerAccount();
        this.siteId = entity.getSiteId();
        this.deviceId = entity.getDeviceId();
        this.echoFw = entity.getEchoFw(); 
        this.echoDatasets = entity.getEchoDatasets();
        this.echoBacklog = entity.getEchoBacklog(); 
        this.echoRadioFail = entity.getEchoRadioFail();
        this.echoTerr = entity.getEchoTerr(); 
        this.echoRestart = entity.getEchoRestart();
        this.echoPowLevel = entity.getEchoPowLevel(); 
        this.echoReboot = entity.getEchoReboot();
        this.echoPowerfault = entity.getEchoPowerfault();
        this.lastUpdate = entity.getLastUpdate();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEchoFw() {
        return echoFw;
    }

    public void setEchoFw(String echoFw) {
        this.echoFw = echoFw;
    }

    public short getEchoDatasets() {
        return echoDatasets;
    }

    public void setEchoDatasets(short echoDatasets) {
        this.echoDatasets = echoDatasets;
    }

    public short getEchoBacklog() {
        return echoBacklog;
    }

    public void setEchoBacklog(short echoBacklog) {
        this.echoBacklog = echoBacklog;
    }

    public byte getEchoRadioFail() {
        return echoRadioFail;
    }

    public void setEchoRadioFail(byte echoRadioFail) {
        this.echoRadioFail = echoRadioFail;
    }

    public byte getEchoTerr() {
        return echoTerr;
    }

    public void setEchoTerr(byte echoTerr) {
        this.echoTerr = echoTerr;
    }

    public short getEchoRestart() {
        return echoRestart;
    }

    public void setEchoRestart(short echoRestart) {
        this.echoRestart = echoRestart;
    }

    public byte getEchoPowLevel() {
        return echoPowLevel;
    }

    public void setEchoPowLevel(byte echoPowLevel) {
        this.echoPowLevel = echoPowLevel;
    }

    public short getEchoReboot() {
        return echoReboot;
    }

    public void setEchoReboot(short echoReboot) {
        this.echoReboot = echoReboot;
    }

    public short getEchoPowerfault() {
        return echoPowerfault;
    }

    public void setEchoPowerfault(short echoPowerfault) {
        this.echoPowerfault = echoPowerfault;
    }

    public Instant getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Instant lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.customerAccount);
        hash = 29 * hash + Objects.hashCode(this.siteId);
        hash = 29 * hash + Objects.hashCode(this.deviceId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EchoDiagnosticsTestEntity other = (EchoDiagnosticsTestEntity) obj;
        if (this.echoFw != other.echoFw) {
            return false;
        }
        if (this.echoDatasets != other.echoDatasets) {
            return false;
        }
        if (this.echoBacklog != other.echoBacklog) {
            return false;
        }
        if (this.echoRadioFail != other.echoRadioFail) {
            return false;
        }
        if (this.echoTerr != other.echoTerr) {
            return false;
        }
        if (this.echoRestart != other.echoRestart) {
            return false;
        }
        if (this.echoPowLevel != other.echoPowLevel) {
            return false;
        }
        if (this.echoReboot != other.echoReboot) {
            return false;
        }
        if (this.echoPowerfault != other.echoPowerfault) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.lastUpdate, other.lastUpdate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EchoDiagnostics{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", deviceId=" + deviceId + ", echoFw=" + echoFw + ", echoDatasets=" + echoDatasets + ", echoBacklog=" + echoBacklog + ", echoRadioFail=" + echoRadioFail + ", echoTerr=" + echoTerr + ", echoRestart=" + echoRestart + ", echoPowLevel=" + echoPowLevel + ", echoReboot=" + echoReboot + ", echoPowerfault=" + echoPowerfault + ", lastUpdate=" + lastUpdate + "}";
    }

}

